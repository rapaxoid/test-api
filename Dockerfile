FROM microsoft/dotnet:2.2-aspnetcore-runtime AS base
WORKDIR /app
#EXPOSE 80

FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /src

# Copy necessary files and restore as distinct layer
COPY src/Rapax.WebApi/Rapax.WebApi.csproj src/Rapax.WebApi/
COPY src/Rapax.WebApi.Data/Rapax.WebApi.Data.csproj src/Rapax.WebApi.Data/
COPY src/Rapax.WebApi.Domain/Rapax.WebApi.Domain.csproj src/Rapax.WebApi.Domain/

RUN dotnet restore src/Rapax.WebApi/Rapax.WebApi.csproj
COPY . .
WORKDIR /src/src/Rapax.WebApi
RUN dotnet build Rapax.WebApi.csproj -c Release -o /app

FROM build AS publish
RUN dotnet publish Rapax.WebApi.csproj -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .

ENV ASPNETCORE_ENVIRONMENT Development

# Start
ENTRYPOINT ["dotnet", "Rapax.WebApi.dll"]