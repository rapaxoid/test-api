using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Rapax.WebApi.Domain.Notification.Services;
using Rapax.WebApi.Domain.Options;
using Rapax.WebApi.Domain.Security.Services;

namespace Rapax.WebApi.Domain
{
    public static class DomainExtensions
    {
        public static IServiceCollection AddDomainServices(this IServiceCollection services, IConfiguration configuration)
        {
            ConfigureSecurity(services, configuration);

            services.Configure<TokenOptions>(configuration.GetSection(TokenOptions.SectionName));
            services.Configure<EmailOptions>(configuration.GetSection(EmailOptions.SectionName));
            services.Configure<CommonOptions>(configuration.GetSection(CommonOptions.SectionName));

            services.AddTransient<IHashBuilder, DummyHashBuilder>();
            services.AddTransient<IEmailNotifier, SmtpEmailNotifier>();
            services.AddTransient<IPasswordService, PasswordService>();
            services.AddTransient<ISecurityService, SecurityService>();

            return services;
        }

        private static void ConfigureSecurity(IServiceCollection services, IConfiguration configuration)
        {
            var tokenOptions = configuration.GetSection(TokenOptions.SectionName).Get<TokenOptions>();

            var key = Encoding.ASCII.GetBytes(tokenOptions.Secret);
            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        ValidateLifetime = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });
        }
    }
}