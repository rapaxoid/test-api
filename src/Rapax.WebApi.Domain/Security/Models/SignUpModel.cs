namespace Rapax.WebApi.Domain.Security.Models
{
    public class SignUpModel
    {
        public string Email { get; set; }
    }
}