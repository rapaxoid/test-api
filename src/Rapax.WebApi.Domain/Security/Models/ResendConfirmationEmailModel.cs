namespace Rapax.WebApi.Domain.Security.Models
{
    public class ResendConfirmationEmailModel
    {
        public bool IsSuccess { get; set; }
    }
}