namespace Rapax.WebApi.Domain.Security.Models
{
    public class ConfirmRegistrationModel
    {
        public bool IsConfirmed { get; set; }
    }
}