namespace Rapax.WebApi.Domain.Security.Services
{
    public interface IPasswordService
    {
        string CreateHash(string password);

        bool IsValid(string password, string correctHash);
    }
}