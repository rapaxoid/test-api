using System;

namespace Rapax.WebApi.Domain.Security.Services
{
    public class DummyHashBuilder : IHashBuilder
    {
        // Just for quiz project !!!
        public string Build()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}