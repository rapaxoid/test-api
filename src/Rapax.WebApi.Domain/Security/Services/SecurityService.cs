using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Rapax.WebApi.Data;
using Rapax.WebApi.Data.Entities;
using Rapax.WebApi.Domain.Common;
using Rapax.WebApi.Domain.Exceptions;
using Rapax.WebApi.Domain.Notification.Services;
using Rapax.WebApi.Domain.Options;
using Rapax.WebApi.Domain.Security.Models;

namespace Rapax.WebApi.Domain.Security.Services
{
    public class SecurityService : ISecurityService
    {
        private readonly IPasswordService _passwordService;
        private readonly IHashBuilder _hashBuilder;
        private readonly IEmailNotifier _emailNotifier;
        private readonly ILogger _logger;
        
        private readonly WebApiDataContext _context;
        private readonly TokenOptions _tokenOptions;
        private readonly CommonOptions _commonOptions;
        
        public SecurityService(
            IPasswordService passwordService,
            IHashBuilder hashBuilder,
            IEmailNotifier emailNotifier,
            ILogger<SecurityService> logger,
            IOptions<TokenOptions> tokenOptions,
            IOptions<CommonOptions> commonOptions,
            WebApiDataContext context)
        {
            _passwordService = passwordService;
            _context = context;
            _tokenOptions = tokenOptions.Value;
            _commonOptions = commonOptions.Value;
            _hashBuilder = hashBuilder;
            _emailNotifier = emailNotifier;
            _logger = logger;
        }
        
        public async Task<TokenModel> SignInAsync(string email, string password)
        {
            Checker.ThrowIfNullOrEmpty(email, nameof(email));
            Checker.ThrowIfNullOrEmpty(password, nameof(password));
            
            using (_logger.BeginScope("An attempt to sign in for user {email}", email))
            {
                var user = await _context.Users
                    .AsNoTracking()
                    .SingleOrDefaultAsync(x => x.Email.Equals(email, StringComparison.OrdinalIgnoreCase));
            
                if (user == null)
                {
                    var message = $"The user with email = '{email}' not found.";
                    _logger.LogInformation(message);
                    throw new NotFoundException(message);
                }

                if (!user.IsConfirmed)
                {
                    var message = "Your account is not confirmed. Please confirm the one to work with the application.";
                    _logger.LogInformation(message);
                    throw new ConflictException(message);
                }

                bool isPasswordCorrect = _passwordService.IsValid(password, user.Password);
                if (!isPasswordCorrect)
                {
                    var message = "The password you provided is not correct.";
                    _logger.LogInformation(message);
                    throw new ConflictException(message);
                }
            
                string token = CreateJwtToken(user);

                _logger.LogInformation("User {email} successfully signed in.", email);
                var model = new TokenModel
                {
                    AccessToken = token,
                    Email = user.Email
                };

                return model;
            }
        }

        private string CreateJwtToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_tokenOptions.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Email, user.Email),
                }),
                Expires = DateTime.UtcNow.AddSeconds(_tokenOptions.ExpiredInSeconds),
                SigningCredentials =
                    new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public async Task<SignUpModel> SignUpAsync(string email, string password)
        {
            Checker.ThrowIfNullOrEmpty(email, nameof(email));
            Checker.ThrowIfNullOrEmpty(password, nameof(password));
            
            using (_logger.BeginScope("An attempt to sign up for user {email}", email))
            {
                _logger.LogInformation($"Checking that user with email '{email}' doesn't exist.");
                var user = await _context.Users
                    .AsNoTracking()
                    .SingleOrDefaultAsync(x =>x.Email.Equals(email, StringComparison.OrdinalIgnoreCase));

                if (user != null)
                {
                    var message = $"The user '{email}' already exists in the application.";
                    _logger.LogError(message);
                    throw new ConflictException(message);
                }
                
                var hashedPassword = _passwordService.CreateHash(password);
                var confirmationHash = _hashBuilder.Build();
                
                _logger.LogInformation($"Creating new account for user with email '{email}'.");
                
                user = new User
                {
                    Email = email,
                    Password = hashedPassword,
                    IsConfirmed = false,
                    Hashes = new List<Hash>
                    {
                        new Hash
                        {
                            Value = confirmationHash,
                            ValidUntil = DateTime.UtcNow.AddDays(7)
                        }
                    }
                };
                
                await _context.Users.AddAsync(user);
                await _context.SaveChangesAsync();
                
                _logger.LogInformation($"Account for user with email '{email}' created.");

                _logger.LogInformation($"Sending confirm registration email.");
                
                await SendConfirmationMessage(confirmationHash, user.Email);

                _logger.LogInformation($"The confirm registration email sent." +
                                       $" The account for user with email '{email}' created.");
            }
            
            return new SignUpModel
            {
                Email = email
            };
        }

        private async Task SendConfirmationMessage(string confirmationHash, string email)
        {
            var body = BuildRegistrationEmail(confirmationHash);
            await _emailNotifier.SendEmailAsync(
                email,
                "noreply@quiz.info",
                "Confirm your registration",
                body);
        }

        public async Task<ConfirmRegistrationModel> ConfirmRegistrationAsync(string token)
        {
            Checker.ThrowIfNullOrEmpty(token, nameof(token));
            
            using (_logger.BeginScope("An attempt to confirm registration"))
            {
                _logger.LogInformation("Getting token information from data storage.");
                var entity = await _context.Hashes
                    .Include(x => x.User)
                    .SingleOrDefaultAsync(x => x.Value == token);

                if (entity == null)
                {
                    var message = $"The token '{token}' is absent";
                    _logger.LogError(message);
                    throw new NotFoundException(message);
                }

                if (entity.ValidUntil < DateTime.UtcNow)
                {
                    var message = $"The token '{token}' is expired. Please contact with the support.";
                    _logger.LogError(message);
                    throw new ConflictException(message);
                }

                entity.User.IsConfirmed = true;
                _context.Hashes.Remove(entity);
                
                var email = entity.User.Email;

                await _context.SaveChangesAsync();
                _logger.LogInformation($"The account for user with email '{email}' confirmed.");
            }

            var model = new ConfirmRegistrationModel {IsConfirmed = true};

            return model;
        }

        public async Task<ResendConfirmationEmailModel> ResendConfirmationEmail(string email)
        {
            Checker.ThrowIfNullOrEmpty(email, nameof(email));

            User user = await _context.Users
                .Include(x => x.Hashes)
                .FirstOrDefaultAsync(x => x.Email == email);

            if (user == null)
            {
                var message = $"The user with email = '{email}' not found.";
                _logger.LogInformation(message);
                throw new NotFoundException(message);
            }

            if (user.IsConfirmed)
            {
                var message = "Your account is already confirmed.";
                _logger.LogInformation(message);
                throw new ConflictException(message);
            }

            var hash = user.Hashes.FirstOrDefault();
            if (hash != null)
            {
                user.Hashes.Remove(hash);
            }

            var hashValue = _hashBuilder.Build();
            hash = new Hash
            {
                Value = hashValue,
                ValidUntil = DateTime.UtcNow.AddDays(7)
            };
            
            user.Hashes.Add(hash);

            await _context.SaveChangesAsync();
            await SendConfirmationMessage(hashValue, user.Email);

            return new ResendConfirmationEmailModel
            {
                IsSuccess = true
            };
        }

        private string BuildRegistrationEmail(string hash)
        {
            StringBuilder body = new StringBuilder();
            body.AppendLine($"Dear user,\n");
            body.AppendLine($"Your account has been created. To activate the one you have to follow to the link bellow: \n");
            body.AppendLine($"{_commonOptions.AppUrl}/confirm/{hash}");
            body.AppendLine($"Best Regards\n");

            return body.ToString();
        }
    }
}