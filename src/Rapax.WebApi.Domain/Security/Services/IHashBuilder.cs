namespace Rapax.WebApi.Domain.Security.Services
{
    public interface IHashBuilder
    {
        string Build();
    }
}