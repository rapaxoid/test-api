using System.Threading.Tasks;
using Rapax.WebApi.Domain.Security.Models;

namespace Rapax.WebApi.Domain.Security.Services
{
    public interface ISecurityService
    {
        Task<TokenModel> SignInAsync(string email, string password);

        Task<SignUpModel> SignUpAsync(string email, string password);

        Task<ConfirmRegistrationModel> ConfirmRegistrationAsync(string token);
        
        Task<ResendConfirmationEmailModel> ResendConfirmationEmail(string modelEmail);
    }
}