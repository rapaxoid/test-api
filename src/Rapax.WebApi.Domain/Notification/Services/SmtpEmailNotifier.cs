using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Polly;
using Rapax.WebApi.Domain.Options;

namespace Rapax.WebApi.Domain.Notification.Services
{
    public class SmtpEmailNotifier : IEmailNotifier
    {
        private readonly EmailOptions _emailOptions;
        private readonly ILogger _logger;
        
        public SmtpEmailNotifier(IOptions<EmailOptions> emailOptions, ILogger<SmtpEmailNotifier> logger)
        {
            _emailOptions = emailOptions.Value;
            _logger = logger;
        }
        
        public async Task SendEmailAsync(string to, string @from, string subject, string body)
        {
            using (var client = new SmtpClient(_emailOptions.Server, _emailOptions.Port))
            {
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(_emailOptions.UserName, _emailOptions.Password);

                var mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(from);
                mailMessage.To.Add(to);
                mailMessage.Body = body;
                mailMessage.Subject = subject;

                await Policy
                    .Handle<Exception>()
                    .WaitAndRetryAsync(
                        _emailOptions.RetryCount,
                        r => TimeSpan.FromSeconds(_emailOptions.SleepDuration),
                        (ex, ts) =>
                        {
                            _logger.LogError($"Error sending mail. Retrying in {_emailOptions.SleepDuration} sec.");
                        })
                    .ExecuteAsync(() => client.SendMailAsync(mailMessage))
                    .ContinueWith(_ => _logger.LogInformation($"Notification mail sent to {to}."));
            }
        }
    }
}