using System.Threading.Tasks;

namespace Rapax.WebApi.Domain.Notification.Services
{
    public interface IEmailNotifier
    {
        Task SendEmailAsync(string to, string from, string subject, string body);
    }
}