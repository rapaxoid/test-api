using System;

namespace Rapax.WebApi.Domain.Common
{
    internal static class Checker
    {
        public static void ThrowIfNullOrEmpty(string argument, string argumentName)
        {
            if (string.IsNullOrWhiteSpace(argument))
            {
                throw new ArgumentNullException(argumentName);
            }
        }
    }
}