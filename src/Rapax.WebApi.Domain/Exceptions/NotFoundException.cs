using System;

namespace Rapax.WebApi.Domain.Exceptions
{
    [Serializable]
    public class NotFoundException : Exception
    {
        public NotFoundException(string message)
            :base(message)
        {
        }
    }
}