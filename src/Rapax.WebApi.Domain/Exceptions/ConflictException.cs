using System;

namespace Rapax.WebApi.Domain.Exceptions
{
    [Serializable]
    public class ConflictException : Exception
    {
        public ConflictException(string message)
            :base(message)
        {
        }
    }
}