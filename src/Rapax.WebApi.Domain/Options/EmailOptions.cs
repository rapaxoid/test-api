namespace Rapax.WebApi.Domain.Options
{
    public class EmailOptions
    {
        public static readonly string SectionName = "Email";
        
        public string Server { get; set; }

        public int Port { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public int RetryCount { get; set; } = 3;

        public int SleepDuration { get; set; } = 2;
    }
}