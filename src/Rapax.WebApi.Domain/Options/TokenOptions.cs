namespace Rapax.WebApi.Domain.Options
{
    public class TokenOptions
    {
        public static readonly string SectionName = "Token";
        
        public string Secret { get; set; }
        
        public int ExpiredInSeconds { get; set; }
    }
}