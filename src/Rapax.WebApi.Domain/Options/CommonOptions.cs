namespace Rapax.WebApi.Domain.Options
{
    public class CommonOptions
    {
        public static readonly string SectionName = "Common";
        
        public string AppUrl { get; set; }
    }
}