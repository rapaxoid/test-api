using System;

namespace Rapax.WebApi.Data.Entities
{
    public class Hash
    {
        public int Id { get; set; }
       
        public string Value { get; set; }
        
        public int UserId { get; set; }
        
        public DateTime ValidUntil { get; set; }
        
        public virtual User User { get; set; }
    }
}