using System.Collections.Generic;

namespace Rapax.WebApi.Data.Entities
{
    public class User
    {
        public int Id { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }
        
        public bool IsConfirmed { get; set; }
        
        public virtual ICollection<Hash> Hashes { get; set; }
    }
}