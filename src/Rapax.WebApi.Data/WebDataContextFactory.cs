using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Rapax.WebApi.Data
{
    public class WebDataContextFactory : IDesignTimeDbContextFactory<WebApiDataContext>
    {
        public WebApiDataContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile($"appsettings.json")
                .Build();
            
            var builder = new DbContextOptionsBuilder<WebApiDataContext>();
            
            builder.UseSqlite(configuration.GetConnectionString("WebApiData"));
 
            return new WebApiDataContext(builder.Options);
        }
    }
}