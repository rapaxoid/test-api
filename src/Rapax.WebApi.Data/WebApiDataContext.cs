using Microsoft.EntityFrameworkCore;
using Rapax.WebApi.Data.Configurations;
using Rapax.WebApi.Data.Entities;

namespace Rapax.WebApi.Data
{
    public class WebApiDataContext : DbContext
    {
        public WebApiDataContext(DbContextOptions<WebApiDataContext> options)
            :base(options)
        {
        }
        
        public DbSet<User> Users { get; set; }
        
        public DbSet<Hash> Hashes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new HashConfiguration());
            
            base.OnModelCreating(modelBuilder);
        }
    }
}