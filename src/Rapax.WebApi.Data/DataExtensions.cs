using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Rapax.WebApi.Data;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class DataExtensions
    {
        public static IServiceCollection AddDataServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<WebApiDataContext>(
                options =>
                {
                    options.UseSqlite(
                        configuration.GetConnectionString("WebApiData"),
                        x => x.MigrationsAssembly(typeof(WebApiDataContext).Assembly.FullName));
                });

            services.AddScoped<WebApiDataContext>();

            return services;
        }
    }
}