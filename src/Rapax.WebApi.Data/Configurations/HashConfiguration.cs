using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Rapax.WebApi.Data.Entities;

namespace Rapax.WebApi.Data.Configurations
{
    public class HashConfiguration : IEntityTypeConfiguration<Hash>
    {
        public void Configure(EntityTypeBuilder<Hash> builder)
        {
            builder.ToTable(nameof(Hash).ToLowerInvariant());

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("INTEGER")
                .IsRequired();

            builder.Property(x => x.UserId)
                .HasColumnName("user_id")
                .HasColumnType("INTEGER")
                .IsRequired();

            builder.Property(x => x.Value)
                .HasColumnName("value")
                .HasColumnType("TEXT")
                .HasMaxLength(512)
                .IsRequired();

            builder.Property(x => x.ValidUntil)
                .HasColumnName("valid_until")
                .HasColumnType("INTEGER")
                .IsRequired();

            builder.HasOne(x => x.User)
                .WithMany(u => u.Hashes)
                .HasForeignKey(x => x.UserId)
                .IsRequired();
        }
    }
}