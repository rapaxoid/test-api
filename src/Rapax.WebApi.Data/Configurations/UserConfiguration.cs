using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Rapax.WebApi.Data.Entities;

namespace Rapax.WebApi.Data.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(nameof(User).ToLowerInvariant());

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasColumnName("id")
                .HasColumnType("INTEGER")
                .IsRequired();

            builder.Property(x => x.Email)
                .HasColumnName("email")
                .HasColumnType("TEXT")
                .IsRequired();

            builder.Property(x => x.Password)
                .HasColumnName("password")
                .HasColumnType("TEXT")
                .IsRequired();

            builder.Property(x => x.IsConfirmed)
                .HasColumnName("is_confirmed")
                .HasColumnType("INTEGER")
                .HasDefaultValue(0);
            
            builder.HasIndex(x => x.Email).IsUnique();

        }
    }
}