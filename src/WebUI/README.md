# Quiz UI Web Application

## Usage

### 1. Clone repository

```
git clone https://gitlab.com/rapaxoid/test-api.git
```

### 2. Instal dependencies

```
npm install
```

or

```
yarn install
```

Open `.env.development` file and specify the API address.


### 3. Run the App

```
npm run start
```

or

```
yarn start
```

App will be opened in browser at http://localhost:3000/

### 4. Build app for production


```
npm run build
```

or

```
yarn build
```

P.S.: One thing was not implemented - unable to confirm registration via UI, but it is possible via API.
Steps to confirm registration:

1. Run backend in docker (with docker-compose)
2. Register new user in UI
3. Open `http://localhost:4000` in browser to open dev mail server
4. Find a registration email
5. Open `http://localhost:5100/swagger` and call `Confirm` method in Security controller. Use token from email.
