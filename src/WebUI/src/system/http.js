import axios from 'axios'
import { apiUrl } from '../system/env'
import {logOut} from "./actions";

export const http = axios.create({
    baseURL: apiUrl + '/api/',
    headers: {
        'Content-Type': 'application/json'
    },
});

http.interceptors.request.use(
    (config) => {
        const accessToken = getStorageData().accessToken;
        if (accessToken) {
            config.headers.authorization = `Bearer ${accessToken}`;
        }
        return config;
    },
    (error) => Promise.reject(error),
);

http.interceptors.response.use(
    response => response,
    async (error) => {
        if (error.status === 401) {
            logOut();
        }

        return Promise.reject(error);
    }
);

export function getStorageData() {
    return {
        accessToken: localStorage.getItem('accessToken'),
    }
}

export function setStorageData(accessToken) {
    localStorage.setItem('accessToken', accessToken);
}

export function clearStorageData() {
    localStorage.removeItem('accessToken');
}
