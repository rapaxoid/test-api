import { createAction } from 'redux-act';
import { http, setStorageData, clearStorageData, getStorageData } from './http'

export const initializeSystem = () => async (dispatch, getState) => {
    const accessToken = getStorageData().accessToken;
    if (accessToken) {
        dispatch(systemIsLoggedIn())
    }
};

export const signIn = (email, password) => async (dispatch, getState) => {
    const { data } = await http.post(
        'security/sign-in',
        { email, password });    
    const { accessToken } = data;
    setStorageData(accessToken);
    dispatch(systemIsLoggedIn())
};

export const logOut = () => (dispatch) => {
    clearStorageData();
    dispatch(systemIsLoggedOut())
};

export const systemIsLoggedIn = createAction('System:LoggedIn');
export const systemIsLoggedOut = createAction('System:LoggedOut');
