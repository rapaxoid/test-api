import React from 'react';
import { Loader } from '../../components/Loader';

export const WithLoading = WrappedComponent => props => props.isLoading 
    ? <Loader /> 
    : <WrappedComponent {...props} />;