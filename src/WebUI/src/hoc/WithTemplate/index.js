import React from 'react';

import { Template } from '../../components/Template';

export const WithTemplate = WrappedComponent => props => props.isLoading 
                ? <Template /> 
                : <WrappedComponent {...props} />;