import { connect } from 'react-redux';

import { SecuredData as Component } from './Component';
import { getSecuredItems } from './actions';

const mapStateToProps = (state) => ({
    securedResources: state.scenes.securedResources,
});

export const SecuredData = connect(mapStateToProps, { getSecuredItems })(Component);