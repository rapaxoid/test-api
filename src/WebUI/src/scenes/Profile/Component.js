import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components //
import { PageTitle } from '../../components/PageTitle';
import PublicItems from '../../components/PublicItems';

export class SecuredData extends Component {
    componentDidMount() {
        this.props.getSecuredItems();
    }

    render() {
        const { isLoading, error, items } = this.props.securedResources;

        return (
            <section className='section'>
                <PageTitle className='section__title'>Закрытые данные</PageTitle>
                <PublicItems isLoading={isLoading} items={items} errorMessage={error} />
            </section>
        )
    }
}

SecuredData.propTypes = {
    getSecuredItems: PropTypes.func.isRequired,
};