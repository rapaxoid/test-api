import { createReducer } from 'redux-act'

import {
    securedResourcesLoadingHasBeenStarted,
    securedResourcesHasBeenLoaded,
    securedResourcesLoadingHasBeenFailed
} from './actions'

const initialState = {
    items: [],
    isLoading: false,
    errorMessage: undefined,
};

export const securedResources = createReducer(
    {

        [securedResourcesLoadingHasBeenStarted]:
            (state) => ({ ...state, isLoading: true, errorMessage: undefined }),

        [securedResourcesHasBeenLoaded]:
            (state, items) => ({ ...state, isLoading: false, items: items, errorMessage: undefined}),

        [securedResourcesLoadingHasBeenFailed]:
            (state, errorMessage) => ({ ...state, isLoading: false, errorMessage }),
    },
    initialState
);
