import {createAction} from "redux-act";
import {http} from "../../system/http";

export const getSecuredItems = () => async(dispatch, getState) => {
    dispatch(securedResourcesLoadingHasBeenStarted());

    try {
        const response = await http.get('resource/secured');
        dispatch(securedResourcesHasBeenLoaded(response.data.items));
    } catch (e) {        
        dispatch(securedResourcesLoadingHasBeenFailed(e.response.data.detail));
    }
};

export const securedResourcesLoadingHasBeenStarted = createAction('SecuredResources:securedResourcesLoadingHasBeenStarted');
export const securedResourcesHasBeenLoaded = createAction('SecuredResources:securedResourcesHasBeenLoaded');
export const securedResourcesLoadingHasBeenFailed = createAction('SecuredResources:securedResourcesLoadingHasBeenFailed');