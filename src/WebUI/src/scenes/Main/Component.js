import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Components //
import { PageTitle } from '../../components/PageTitle';
import PublicItems from '../../components/PublicItems';

export class PublicData extends Component {
    componentDidMount() {
        this.props.getPublicItems();
    }

    render() {        
        const { isLoading, error, items } = this.props.publicResources;        
        
        return (
            <section className='section'>
                <PageTitle className='section__title'>Открытые данные</PageTitle>
                <PublicItems isLoading={isLoading} items={items} errorMessage={error} />
            </section>
        )
    }
}

PublicData.propTypes = {
    getPublicItems: PropTypes.func.isRequired,    
};