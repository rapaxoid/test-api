import {createAction} from "redux-act";
import {http} from "../../system/http";

export const getPublicItems = () => async(dispatch, getState) => {
    dispatch(publicResourcesLoadingHasBeenStarted());

    try {
        const response = await http.get('resource/public');        
        dispatch(publicResourcesHasBeenLoaded(response.data));
    } catch (e) {
        dispatch(publicResourcesLoadingHasBeenFailed(e.response.data.message));
    }   
};

export const publicResourcesLoadingHasBeenStarted = createAction('PublicResources:publicResourcesLoadingHasBeenStarted');
export const publicResourcesHasBeenLoaded = createAction('PublicResources:publicResourcesHasBeenLoaded');
export const publicResourcesLoadingHasBeenFailed = createAction('PublicResources:publicResourcesLoadingHasBeenFailed');