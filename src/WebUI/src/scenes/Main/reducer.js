import { createReducer } from 'redux-act'

import {
    publicResourcesLoadingHasBeenStarted,
    publicResourcesHasBeenLoaded,
    publicResourcesLoadingHasBeenFailed
} from './actions'

const initialState = {
    items: [],
    isLoading: false,
    errorMessage: undefined,
};

export const publicResources = createReducer(
    {        

        [publicResourcesLoadingHasBeenStarted]:
            (state) => ({ ...state, isLoading: true, errorMessage: undefined }),

        [publicResourcesHasBeenLoaded]:
            (state, items) => ({ ...state, isLoading: false, items: items, errorMessage: undefined}),

        [publicResourcesLoadingHasBeenFailed]:
            (state, errorMessage) => ({ ...state, isLoading: false, errorMessage }),
    },
    initialState
);
