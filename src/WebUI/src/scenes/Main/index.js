import { connect } from 'react-redux';

import { PublicData as Component } from './Component';
import { getPublicItems } from './actions';

const mapStateToProps = (state) => ({
    publicResources: state.scenes.publicResources,
});

export const PublicData = connect(mapStateToProps, { getPublicItems })(Component);