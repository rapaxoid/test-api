import { createReducer } from 'redux-act'

import {
    formIsChanged,
    signingUpHasBeenFailed,
    signingUpHasBeenStarted,
    signingUpHasBeenSucceed
} from './actions'

const initialState = {
    email: '',
    password: '',
    emailError: '',
    passwordError: '',
    isLoading: false,
    isLogged: false,
    submitButtonDisabled: true,
    errorMessage: undefined,
};

export const signUp = createReducer(
    {
        [formIsChanged]:
            (state, formPart) => ({ ...state, ...formPart }),

        [signingUpHasBeenStarted]:
            (state) => ({ ...state, isLoading: true, errorMessage: undefined, isLogged: false }),

        [signingUpHasBeenSucceed]:
            () => ({ ...initialState, isLogged: true }),

        [signingUpHasBeenFailed]:
            (state, errorMessage) => ({ ...state, isLoading: false, errorMessage, isLogged: false }),
    },
    initialState
);