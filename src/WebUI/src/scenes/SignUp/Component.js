import React, { Component } from 'react';

import { PageTitle } from '../../components/PageTitle';
import { SignupForm } from '../../components/SignUpForm';

export class SignUp extends Component {
    render() {
        return (
            <section className='section'>
                <PageTitle className='section__title'>Регистрация пользователя</PageTitle>
                <div className='row center-xs'>
                    <div className='col-xs-12 col-sm-6 col-md-4'>
                        <SignupForm
                            className='section__form'
                            {...this.props}
                        />
                    </div>
                </div>
            </section>
        );
    }
}