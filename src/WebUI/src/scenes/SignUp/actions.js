import { createAction } from 'redux-act';
import { http } from '../../system/http';

export const formIsChanged = createAction('SignUpScene:formIsChanged');
export const signingUpHasBeenStarted = createAction('SignUpScene:signingUpHasBeenStarted');
export const signingUpHasBeenSucceed = createAction('SignUpScene:signingUpHasBeenSucceed');
export const signingUpHasBeenFailed = createAction('SignUpScene:signingUpHasBeenFailed');

export const signUp = () => async (dispatch, getState) => {
    dispatch(signingUpHasBeenStarted());
    const { email, password } = getState().scenes.signUp;
    try {
        await http.post('security/sign-up', {email, password});
        dispatch(signingUpHasBeenSucceed())
    } catch (e) {
        console.log(e);
        const { data } = e.response;
        dispatch(signingUpHasBeenFailed(data.detail))
    }
};