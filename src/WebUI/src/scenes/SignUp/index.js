import { SignUp as Component } from './Component';
import { signUp, formIsChanged } from './actions';
import {connect} from "react-redux";

const mapStateToProps = state => ({
    signUpScene : state.scenes.signUp,
});

export const SignUp = connect(mapStateToProps, { signUp, formIsChanged  })(Component);