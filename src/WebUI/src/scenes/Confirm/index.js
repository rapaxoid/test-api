import { connect } from 'react-redux';

import { Confirm as Component } from './Component';
import { confirmAccount } from './actions';

const mapStateToProps = (state) => ({
    confirm: state.scenes.confirm,
});

export const Confirm = connect(mapStateToProps, { confirmAccount })(Component);