import React, { Component } from 'react';
import { PageTitle } from '../../components/PageTitle';

export class Confirm extends Component {
    componentDidMount() {
        const { params } = this.props.match;

        const {confirmAccount} = this.props;
        
        if(params && params.hash){
            confirmAccount(params.hash)
                .then(() => {
                    this.props.history.push('/sign-in');
                });                
        }
    }
    
    render() {
        return (
            <section className='section'>
                <PageTitle className='section__title'>Активация аккаунта</PageTitle>                
            </section>
        )
    }
}