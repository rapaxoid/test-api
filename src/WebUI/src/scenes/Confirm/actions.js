import {createAction} from "redux-act";
import {http} from "../../system/http";

export const confirmAccount = (token) => async(dispatch, getState) => {
    dispatch(accountConfirmationHasBeenStarted());

    try {
        const response = await http.post('security/confirm', {token});
        dispatch(accountConfirmationHasBeenCompleted(response.data));
    } catch (e) {
        dispatch(accountConfirmationHasBeenFailed(e.response.data.message));
    }
};

export const accountConfirmationHasBeenStarted = createAction('Confirm:accountConfirmationHasBeenStarted');
export const accountConfirmationHasBeenCompleted = createAction('Confirm:accountConfirmationHasBeenCompleted');
export const accountConfirmationHasBeenFailed = createAction('Confirm:accountConfirmationHasBeenFailed');