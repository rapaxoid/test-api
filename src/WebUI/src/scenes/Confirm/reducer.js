import { createReducer } from 'redux-act'

import {
    accountConfirmationHasBeenStarted,
    accountConfirmationHasBeenCompleted,
    accountConfirmationHasBeenFailed
} from './actions'

const initialState = {    
    isLoading: false,
    errorMessage: undefined,
};

export const confirm = createReducer(
    {

        [accountConfirmationHasBeenStarted]:
            (state) => ({ ...state, isLoading: true, errorMessage: undefined }),

        [accountConfirmationHasBeenCompleted]:
            (state) => ({ ...state, isLoading: false, errorMessage: undefined}),

        [accountConfirmationHasBeenFailed]:
            (state, errorMessage) => ({ ...state, isLoading: false, errorMessage }),
    },
    initialState
);