import React from 'react';
import { PageTitle } from '../../components/PageTitle';

export const NotFound = () => (
    <section className='section'>
        <PageTitle className='section__title'>Страница не найдена :(</PageTitle>
        <p>Четыреста чертыре</p>
    </section>
);