import { createReducer } from 'redux-act'

import {
    formIsChanged,
    signingInHasBeenFailed,
    signingInHasBeenStarted,
    signingInHasBeenSucceed,
    resendHasBeenStarted,
    resendHasBeenSucceed,
    resendHasBeenFailed
} from './actions'

const initialState = {
    email: '',
    password: '',
    emailError: '',
    passwordError: '',
    isLoading: false,
    isLogged: false,
    submitButtonDisabled: true,
    errorMessage: undefined,
};

export const signIn = createReducer(
    {
        [formIsChanged]:
            (state, formPart) => ({ ...state, ...formPart }),

        [signingInHasBeenStarted]:
            (state) => ({ ...state, isLoading: true, errorMessage: undefined, isLogged: false }),

        [signingInHasBeenSucceed]:
            () => ({ ...initialState, isLogged: true }),

        [signingInHasBeenFailed]:
            (state, errorMessage) => ({ ...state, isLoading: false, errorMessage, isLogged: false }),
        
        [resendHasBeenStarted]:
            (state) => ({ ...state, isLoading: true, errorMessage: undefined, isLogged: false}),
        
        [resendHasBeenSucceed]:
            () => ({ ...initialState}),
        
        [resendHasBeenFailed]:
            (state, errorMessage) => ({ ...[state], isLoading: false, errorMessage, isLogged: false})
    },
    initialState
);
