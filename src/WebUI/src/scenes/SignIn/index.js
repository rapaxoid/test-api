import { SignIn as Component } from './Component';
import { signIn, formIsChanged, resendConfirmationEmail } from './actions';
import {connect} from "react-redux";

const mapStateToProps = state => ({
    signInScene : state.scenes.signIn,
});

export const SignIn = connect(mapStateToProps, { signIn, formIsChanged, resendConfirmationEmail })(Component);