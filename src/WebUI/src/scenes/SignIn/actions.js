import { createAction } from 'redux-act';
import { signIn as systemSignIn } from '../../system/actions'
import { http } from '../../system/http';

export const formIsChanged = createAction('SignInScene:formIsChanged');
export const signingInHasBeenStarted = createAction('SignInScene:signingInHasBeenStarted');
export const signingInHasBeenSucceed = createAction('SignInScene:signingInHasBeenSucceed');
export const signingInHasBeenFailed = createAction('SignInScene:signingInHasBeenFailed');

export const resendHasBeenStarted = createAction('SignInScene:resendHasBeenStarted');
export const resendHasBeenSucceed = createAction('SignInScene:resendHasBeenSucceed');
export const resendHasBeenFailed = createAction('SignInScene:resendHasBeenFailed');

export const signIn = () => async (dispatch, getState) => {
    dispatch(signingInHasBeenStarted());
    const { email, password } = getState().scenes.signIn;
    try {
        await systemSignIn(email, password)(dispatch, getState);
        dispatch(signingInHasBeenSucceed())        
    } catch (e) {        
        const { data } = e.response;
        dispatch(signingInHasBeenFailed(data.detail))
    }
};

export const resendConfirmationEmail = (email) => async(dispatch, getState) => {
    dispatch(resendHasBeenStarted());
    
    try {
        await http.post('security/resend', {email});
        dispatch(resendHasBeenSucceed())
    } catch (e) {
        const { data } = e.response;
        dispatch(resendHasBeenFailed(data.detail))
    }
};