import React, { Component } from 'react';

import { PageTitle } from '../../components/PageTitle';
import { SigninForm } from '../../components/SignInForm';

export class SignIn extends Component {
    render() {
        return (
            <section className='section'>
                <PageTitle className='section__title'>Вход</PageTitle>
                <div className='row center-xs'>
                    <div className='col-xs-12 col-sm-6 col-md-4'>
                        <SigninForm
                            className='section__form'
                            {...this.props}                            
                        />
                    </div>
                </div>
            </section>
        );
    }
}