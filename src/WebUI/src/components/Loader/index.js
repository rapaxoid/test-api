import React from 'react';

import './styles.css';

export const Loader = () => (
    <div className="loader">
        <div className="loader__box"></div>
    </div>
);