import React from 'react';

import './styles.css';

export const Template = () => (
    <div className='row center-xs'>
        <div className='col-xs-12 col-md-8'>
            <div className='template'>
                <div className='template__item'>
                    <div className='template__title'></div>
                    <div>
                        <div className='template__text'></div>
                        <div className='template__text'></div>
                        <div className='template__text'></div>
                    </div>
                </div>
                <div className='template__item'>
                    <div className='template__title'></div>
                    <div>
                        <div className='template__text'></div>
                        <div className='template__text'></div>
                        <div className='template__text'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
);