import React from 'react';
import PropTypes from 'prop-types';

// Styles //

import './styles.css';

export const PublicItem = ({ data }) => (
    <article className='news-item'>
        <div className='news-item__content'>
            <h1 className='news-item__title'>{data.name}</h1>
            <p className='news-item__preview'>{data.description}</p>
        </div>
    </article>
);

PublicItem.propTypes = {
    data: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
    }).isRequired,
};