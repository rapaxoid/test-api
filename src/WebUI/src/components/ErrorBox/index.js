import React from 'react';
import PropTypes from 'prop-types';

// Styles

import './styles.css';

export const ErrorBox = props => (
    <div className='errors-box'>
        <p className='errors-box__item'>{props.message}</p>
    </div>
);

ErrorBox.propTypes = {
    message: PropTypes.string.isRequired,
};