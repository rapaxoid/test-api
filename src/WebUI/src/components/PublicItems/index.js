import React from 'react';
import PropTypes from 'prop-types';

// Components

import { PublicItem } from '../PublicItem';
import { WithTemplate } from '../../hoc/WithTemplate';
import { ErrorBox } from "../ErrorBox";

const PublicItems = ({ errorMessage, items }) => (
    <div className='row center-xs'>
        <div className='col-xs-12 col-md-8'>
            { errorMessage && <ErrorBox message={errorMessage} /> }
            {
                items.map(item => <PublicItem key={item.id} data={item} />)
            }
            { !errorMessage && <p>Всего записей: {items.length}</p> }
        </div>
    </div>
);

PublicItems.propTypes = {
    errorMessage: PropTypes.string,
    items: PropTypes.array,
};

export default WithTemplate(PublicItems);