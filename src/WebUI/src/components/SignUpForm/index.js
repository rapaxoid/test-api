import React, { Component } from 'react';
import PropTypes from 'prop-types';

// Styles //

import './styles.css';

// Components //

import { WithLoading } from '../../hoc/WithLoading';
import { ErrorBox } from '../../components/ErrorBox';

// Actions //

const LoadingButton = WithLoading((props) =>
    <button className='form__submit button' disabled={props.isDisabled} type='submit'>Зарегистрировать</button>
);

export class SignupForm extends Component {

    handleUserInput = (e) => {
        const fieldName = e.target.name;
        const value = e.target.value;

        let {email, emailError, password, passwordError} = this.props.signUpScene;
        let error = '';

        switch(fieldName) {
            case 'email':
                const emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
                error = emailValid ? '' : 'Email is incorrect.';
                this.props.formIsChanged({ email: value, emailError: error, submitButtonDisabled: Boolean(error || passwordError) || password.length === 0 });
                break;
            case 'password':
                const passwordValid = value.length >= 6;
                error = passwordValid ? '' : 'Password is too short.';
                this.props.formIsChanged({ password: value, passwordError: error, submitButtonDisabled: Boolean(error || emailError) || email.length === 0 });
                break;
            default:
                break;
        }
    };

    onSubmit = (event) => {
        event.preventDefault();
        const {email, password, isLoading} = this.props.signUpScene;

        if (!isLoading) {
            const {signUp} = this.props;
            signUp(email, password);
        }
    };

    render() {
        const { email, emailError, passwordError, password, errorMessage, isLoading, submitButtonDisabled } = this.props.signUpScene;        

        const emailClassName = emailError.length === 0
            ? 'form__input'
            : 'form__input error';
        const passwordClassName = passwordError.length === 0
            ? 'form__input'
            : 'form__input error';

        return (
            <form className={`${this.props.className ? this.props.className : ''} form`} onSubmit={this.onSubmit}>                
                <label htmlFor="email">Email: </label>
                <input
                    className={emailClassName}
                    type='email'
                    name='email'
                    onChange={this.handleUserInput}
                    value={email}
                    autoComplete="off"
                    placeholder='Email' />
                <label htmlFor="password">Пароль: </label>
                <input
                    className={passwordClassName}
                    type='password'
                    name='password'
                    onChange={this.handleUserInput}
                    value={password}
                    placeholder='Пароль' />
                { errorMessage && <ErrorBox message={errorMessage} /> }
                <div className="form__footer">
                    <LoadingButton isLoading={isLoading} isDisabled={submitButtonDisabled} />
                </div>
            </form>
        );
    }
}

SignupForm.propTypes = {
    className: PropTypes.string,
    signUp: PropTypes.func.isRequired,
    formIsChanged: PropTypes.func.isRequired,
    signUpScene: PropTypes.shape()
};