import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

export const SecuredRoute = ({component: Component, allowed, redirect, ...rest}) => (
    <Route
        {...rest}
        render={
            props => allowed
                ? <Component {...props} />
                : <Redirect to={{pathname: redirect, state: {from: props.location}}} />
        }
    />
);

SecuredRoute.propTypes = {
    component: PropTypes.object.isRequired,
    allowed: PropTypes.bool.isRequired,
    path: PropTypes.string.isRequired,
    redirect: PropTypes.string.isRequired,
};