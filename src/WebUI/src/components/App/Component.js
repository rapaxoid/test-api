import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';

import { PublicData } from '../../scenes/Main';
import { SecuredData } from '../../scenes/Profile';
import { SignIn } from '../../scenes/SignIn';
import { SignUp } from '../../scenes/SignUp';
import { NotFound } from '../../scenes/NotFound';
import { Confirm } from '../../scenes/Confirm';

import { Header } from '../../components/Header';
import { SecuredRoute } from '../../components/SecuredRoute';
import { getStorageData } from '../../system/http'


export class App extends Component {
    render() {        
        return (
            <Router>
                <div>
                    <Header />
                    <div className="container">
                        <Switch>
                            <Route exact path='/' component={PublicData} />            
                            <Route exact path='/sign-in' component={ SignIn } />
                            <Route exact path='/sign-up' component={ SignUp } />
                            <Route exact path='/confirm/:hash' component={ Confirm } />
                            <SecuredRoute path='/profile' component={ SecuredData } allowed={!!getStorageData().accessToken} redirect='/sign-in' />                            
                            <Route component={NotFound} />
                        </Switch>
                    </div>
                </div>
            </Router>
        )
    }
}

App.propTypes = {
    systemSignIn: PropTypes.func.isRequired,        
};

