import {connect} from "react-redux";
import {App} from "./Component";

import { signIn as systemSignIn } from '../../system/actions';

const mapStateToProps = state => ({
    signIn: state.scenes.signIn
});

export default connect( mapStateToProps, { systemSignIn })(App);