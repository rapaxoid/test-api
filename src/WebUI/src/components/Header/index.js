import {connect} from "react-redux";
import { logOut } from '../../system/actions';
import { Header as Component } from './Component';
import { withRouter } from 'react-router-dom';

export const Header = withRouter(connect(null, { logOut })(Component));