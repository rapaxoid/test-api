import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import './styles.css';

import { Navbar } from '../NavBar';
import { getStorageData } from '../../system/http';

export class Header extends Component {
    
    handleLogOutClick = (e) => {
        const {logOut, history} = this.props;
        
        logOut();
        (history.push('/'));
    };
    
    render() {
        const isAuthorized = Boolean(getStorageData().accessToken);
        return (
            <header className='header'>
                <div className='container'>
                    <div className="header__inner">
                        <div className="header__box header__box--scrollable">
                            <Navbar/>
                        </div>
                        <div className="header__box">
                            {
                                isAuthorized
                                    ? <button className='header__button button' onClick={this.handleLogOutClick}>Выйти</button>
                                    : <NavLink className='header__button button' exact to='/sign-in'>Войти</NavLink>
                            }
                        </div>
                    </div>
                </div>
            </header>
        );
    }
}

Header.propTypes = {
    logOut: PropTypes.func.isRequired,    
};