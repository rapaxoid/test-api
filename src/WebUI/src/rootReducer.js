import { combineReducers } from 'redux'
import { publicResources } from './scenes/Main/reducer';
import { signIn } from './scenes/SignIn/reducers';
import { signUp } from './scenes/SignUp/reducers';
import { securedResources } from './scenes/Profile/reducer';
import { confirm } from './scenes/Confirm/reducer';

export const rootReducer = combineReducers({    
    scenes: combineReducers({
        publicResources: publicResources,
        signIn : signIn,
        signUp : signUp,
        securedResources: securedResources,
        confirm: confirm
    })
});