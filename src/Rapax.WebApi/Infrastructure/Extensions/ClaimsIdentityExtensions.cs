using System;
using System.Linq;
using System.Security.Claims;

namespace Rapax.WebApi.Infrastructure.Extensions
{
    public static class ClaimsIdentityExtensions
    {
        public static string GetEmail(this ClaimsPrincipal identity)
        {
            return identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Email)?.Value;
        }

        public static int GetId(this ClaimsPrincipal identity)
        {
            Claim claim = identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name);

            return Convert.ToInt32(claim?.Value);
        }
    }
}