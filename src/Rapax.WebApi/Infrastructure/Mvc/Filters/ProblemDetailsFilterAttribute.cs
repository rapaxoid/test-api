using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Rapax.WebApi.Domain.Exceptions;

namespace Rapax.WebApi.Infrastructure.Mvc.Filters
{
    public class ProblemDetailsFilterAttribute : Attribute, IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            (int code, string title) = GetStatusArtifacts(context.Exception);
            context.HttpContext.Response.StatusCode = code;
            
            var problemDetails = new ProblemDetails
            {
                Status = code,
                Detail = context.Exception.Message,
                Title = title,
                Type = $"https://httpstatuses.com/{code}",
            };

            context.Result = new JsonResult(problemDetails);
        }
        
        private static (int code, string title) GetStatusArtifacts(Exception exception)
        {
            HttpStatusCode code;
            string title = string.Empty;

            switch (exception)
            {
                case NotFoundException _:
                    code = HttpStatusCode.NotFound;
                    title = "Requested entity not found.";
                    break;
                case ConflictException _:
                    code = HttpStatusCode.Conflict;
                    title = "One or more conflict errors occured.";
                    break;
                default:
                    title = "One or more unhandled errors occured.";
                    code = HttpStatusCode.InternalServerError;
                    break;
            }
            
            return ((int) code, title);
        }
    }
}