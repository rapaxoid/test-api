using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Rapax.WebApi.Domain.Security.Services;
using Rapax.WebApi.Models.Requests;

namespace Rapax.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SecurityController : ControllerBase
    {
        private readonly ISecurityService _securityService;

        public SecurityController(ISecurityService securityService)
        {
            _securityService = securityService;
        }
        
        /// <summary>
        /// Registers new users in the application
        /// </summary>
        /// <param name="model">Sign up parameters</param>
        /// <returns></returns>
        [HttpPost("sign-up")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.Created)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.Conflict)]
        public async Task<IActionResult> SignUp([FromBody] SignUpRequestModel model)
        {
            var result = await _securityService.SignUpAsync(model.Email, model.Password);

            return Ok(result);
        }

        /// <summary>
        /// Allows to confirm registered users their registration.
        /// </summary>
        /// <param name="model">Confirmation parameters.</param>
        /// <returns></returns>
        [HttpPost("confirm")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.Conflict)]
        public async Task<IActionResult> ConfirmRegistration([FromBody] ConfirmRegistrationRequestModel model)
        {
            var result = await _securityService.ConfirmRegistrationAsync(model.Token);

            return Ok(result);
        }
        
        /// <summary>
        /// Allows users to login to the application with their credentials.
        /// </summary>
        /// <param name="model">Sign in parameters</param>
        /// <returns></returns>
        [HttpPost("sign-in")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> SignIn([FromBody] SignInRequestModel model)
        {
            var result = await _securityService.SignInAsync(model.Email, model.Password);

            return Ok(result);
        }

        [HttpPost("resend")]
        [Produces("application/json")]
        [ProducesResponseType((int) HttpStatusCode.OK)]
        [ProducesResponseType((int) HttpStatusCode.Conflict)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ResendConfirmationEmail([FromBody] ResendRequestModel model)
        {
            var result = await _securityService.ResendConfirmationEmail(model.Email);

            return Ok(result);
        }
    }
}