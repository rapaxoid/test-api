using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rapax.WebApi.Infrastructure.Extensions;

namespace Rapax.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ResourceController : ControllerBase
    {
        /// <summary>
        /// Returns some public resources.
        /// </summary>
        /// <returns></returns>
        [HttpGet("public")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetPublicResources()
        {
            // Await 1 second to emulate work.
            await Task.Delay(1000);

            var result = Enumerable.Range(1, 10)
                .Select(x => new
                {
                    Id = x,
                    Name = $"Public resource #{x}.",
                    Description = $"Description for the public resource #{x}."
                });

            return Ok(result);
        }

        /// <summary>
        /// Returns some secured resources.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpGet("secured")]
        [Produces("application/json")]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetSecuredResources()
        {
            // Await 1 second to emulate work.
            await Task.Delay(1000);
            
            var userId = HttpContext.User.GetId();
            var userEmail = HttpContext.User.GetEmail();

            var result = new
            {
                Id = userId,
                Email = userEmail,
                items = Enumerable.Range(1, 10)
                    .Select(x => new
                    {
                        Id = x,
                        Name = $"Secured resource #{x}.",
                        Description = $"Description for the secured resource #{x}."
                    }) 
            };

            return Ok(result);
        }
    }
}