using System.ComponentModel.DataAnnotations;

namespace Rapax.WebApi.Models.Requests
{
    public abstract class BaseAuthRequestModel
    {
        [Required, EmailAddress]
        public string Email { get; set; }

        [Required, MinLength(6)]
        public string Password { get; set; }
    }
}