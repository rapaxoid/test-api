using System.ComponentModel.DataAnnotations;

namespace Rapax.WebApi.Models.Requests
{
    public class ConfirmRegistrationRequestModel
    {
        [Required]
        public string Token { get; set; }
    }
}