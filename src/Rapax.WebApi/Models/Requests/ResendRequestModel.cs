using System.ComponentModel.DataAnnotations;

namespace Rapax.WebApi.Models.Requests
{
    public class ResendRequestModel
    {
        [Required, EmailAddress]
        public string Email { get; set; }
    }
}