How to build and run docker image

Clone the latest version to your local computer;
.Net Core 2.2 should be installed to your local computer;
Open terminal and go to the sources folder;
Run `dotnet clean` command to clean up;
Run `dotnet restore` command to restore required nuget packages;
Run `dotnet build` command to build the solution;
Run `docker-compose -f docker-compose.yaml up -d` to build docker images and start containers from local folder;
There are three containers should be started:


1.  API
2.  Seq
3.  MailDev

To get an access to swagger documentation
Open browser and try to open http://localhost:5100/swagger

To get an access to web ui of email server
Open browser and try to open http://localhost:4000

To get an access to seq web ui 
Open browser and try to open http://localhost:5341

To remove deployed containers you have to execute the following command:
`docker-compose -f docker-compose.yaml down --rmi all`