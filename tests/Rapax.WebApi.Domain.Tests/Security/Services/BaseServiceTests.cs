using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Moq;
using Rapax.WebApi.Data;
using Rapax.WebApi.Domain.Notification.Services;
using Rapax.WebApi.Domain.Options;
using Rapax.WebApi.Domain.Security.Services;

namespace Rapax.WebApi.Domain.Tests.Security.Services
{
    public class BaseServiceTests
    {
        protected Mock<IPasswordService> PreparePasswordServiceMock()
        {
            Mock<IPasswordService> passwordServiceMock = new Mock<IPasswordService>();

            passwordServiceMock.Setup(m => m.CreateHash(It.IsNotNull<string>())).Returns(It.IsAny<string>());

            return passwordServiceMock;
        }

        protected Mock<IHashBuilder> PrepareHashBuilderMock()
        {
            Mock<IHashBuilder> hashBuilderMock = new Mock<IHashBuilder>();
            hashBuilderMock.Setup(m => m.Build()).Returns(It.IsAny<string>());

            return hashBuilderMock;
        }

        protected Mock<IEmailNotifier> PrepareEmailNotifierMock()
        {
            Mock<IEmailNotifier> emailNotifierMock = new Mock<IEmailNotifier>();
            emailNotifierMock.Setup(m => m.SendEmailAsync(
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>()));

            return emailNotifierMock;
        }

        protected WebApiDataContext PrepareInMemoryDataContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<WebApiDataContext>();
            optionsBuilder.UseInMemoryDatabase("test.db");
            
            return new WebApiDataContext(optionsBuilder.Options);
        }

        protected IOptions<TokenOptions> PrepareTokenOptions()
        {
            return Microsoft.Extensions.Options.Options.Create<TokenOptions>(new TokenOptions
            {
                ExpiredInSeconds = 6000,
                Secret = "XCAP05H6LoKvbRRa/QkqLNMI7cOHguaRyHzyg7n5qEkGjQmtBhz4SzYh4Fqwjyi3KJHlSXKPwVu2+bXr6CtpgQ=="

            });
        }
        
        protected IOptions<CommonOptions> PrepareCommonOptions()
        {
            return Microsoft.Extensions.Options.Options.Create<CommonOptions>(new CommonOptions
            {
                AppUrl = "http://localhost:8888"
            });
        }
    }
}