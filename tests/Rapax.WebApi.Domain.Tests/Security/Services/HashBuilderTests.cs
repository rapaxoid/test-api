using Rapax.WebApi.Domain.Security.Services;
using Xunit;

namespace Rapax.WebApi.Domain.Tests.Security.Services
{
    public class HashBuilderTests
    {
        private IHashBuilder _hashBuilder;

        [Fact]
        public void Build_ShouldReturnNotEmptyOrNull()
        {
            _hashBuilder = new DummyHashBuilder();
            var result = _hashBuilder.Build();
            
            Assert.True(!string.IsNullOrWhiteSpace(result));
        }
    }
}