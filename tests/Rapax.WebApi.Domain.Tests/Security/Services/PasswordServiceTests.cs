using System;
using Rapax.WebApi.Domain.Security.Services;
using Xunit;

namespace Rapax.WebApi.Domain.Tests.Security.Services
{
    public class PasswordServiceTests
    {
        private IPasswordService _passwordService;

        private const string CorrectPassword = "Hello World";
        private const string IncorrectPassword = "Hello World1";

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public void CreateHash_UseEmptyOrNullString_ShouldThrowsArgumentNulException(string input)
        {
            _passwordService = new PasswordService();
            Assert.Throws<ArgumentNullException>(() => _passwordService.CreateHash(input));
        }

        [Fact]
        public void CreateHash_UseNotEmpty_ShouldGenerateHash()
        {
            _passwordService = new PasswordService();
            var hash = _passwordService.CreateHash(CorrectPassword);
            
            Assert.True(!string.IsNullOrWhiteSpace(hash));
        }

        [Theory]
        [InlineData("", "hfjdkhfkdhfkdjf")]
        [InlineData(null, "hfjdkhfkdhfkdjf")]
        public void ValidatePassword_UseEmptyOrNullStringAsPasswordAndCorrectHash_ShouldReturnFalse(string input, string correctHash)
        {
            _passwordService = new PasswordService();
            var result = _passwordService.IsValid(input, correctHash);
            
            Assert.False(result);
        }
        
        [Theory]
        [InlineData("hfjdkhfkdhfkdjf", "")]
        [InlineData("hfjdkhfkdhfkdjf", null)]
        public void ValidatePassword_UsePasswordAndEmptyOrNullHash_ShouldReturnFalse(string input, string correctHash)
        {
            _passwordService = new PasswordService();
            var result = _passwordService.IsValid(input, correctHash);
            
            Assert.False(result);
        }

        [Fact]
        public void ValidatePassword_UseCorrectParameters_ShouldReturnTrue()
        {
            _passwordService = new PasswordService();
            
            string hash = _passwordService.CreateHash(CorrectPassword);

            bool result = _passwordService.IsValid(CorrectPassword, hash);
            
            Assert.True(result);
        }

        [Fact]
        public void ValidatePassword_UseIncorrectPassword_ShouldReturnFalse()
        {
            _passwordService = new PasswordService();
            
            string hash = _passwordService.CreateHash(CorrectPassword);

            bool result = _passwordService.IsValid(IncorrectPassword, hash);
            
            Assert.False(result);
        }
    }
}