using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Moq;
using Rapax.WebApi.Domain.Security.Services;
using Xunit;

namespace Rapax.WebApi.Domain.Tests.Security.Services
{
    public class SecurityServiceTests : BaseServiceTests
    {
        private ISecurityService _securityService;

        #region Check behavior for incorrect input parameters
        
        [Theory]
        [InlineData("test", null)]
        [InlineData("test", "")]
        [InlineData(null, "test")]
        [InlineData("", "test")]
        [InlineData("", "")]
        [InlineData(null, null)]
        public async Task SignUpAsync_UseIncorrectParameters_ShouldThrowsArgumentNullException(string email, string password)
        {
            var passwordServiceStub = PreparePasswordServiceMock();
            var hashBuilderStub = PrepareHashBuilderMock();
            var emailNotifierStub = PrepareEmailNotifierMock();
            Mock<ILogger<SecurityService>> loggerStub = new Mock<ILogger<SecurityService>>();
            
            var dbContext = PrepareInMemoryDataContext();
            var tokenOptions = PrepareTokenOptions();
            var commonOptions = PrepareCommonOptions();

            _securityService = new SecurityService(
                passwordServiceStub.Object,
                hashBuilderStub.Object,
                emailNotifierStub.Object,
                loggerStub.Object,
                tokenOptions,
                commonOptions,
                dbContext
            );

            await Assert.ThrowsAsync<ArgumentNullException>(async () =>  await _securityService.SignUpAsync(email, password));
        }
        
        [Theory]
        [InlineData("test", null)]
        [InlineData("test", "")]
        [InlineData(null, "test")]
        [InlineData("", "test")]
        [InlineData("", "")]
        [InlineData(null, null)]
        public async Task SignInAsync_UseIncorrectParameters_ShouldThrowsArgumentNullException(string email, string password)
        {
            var passwordServiceStub = PreparePasswordServiceMock();
            var hashBuilderStub = PrepareHashBuilderMock();
            var emailNotifierStub = PrepareEmailNotifierMock();
            Mock<ILogger<SecurityService>> loggerStub = new Mock<ILogger<SecurityService>>();
            
            var dbContext = PrepareInMemoryDataContext();
            var tokenOptions = PrepareTokenOptions();
            var commonOptions = PrepareCommonOptions();

            _securityService = new SecurityService(
                passwordServiceStub.Object,
                hashBuilderStub.Object,
                emailNotifierStub.Object,
                loggerStub.Object,
                tokenOptions,
                commonOptions,
                dbContext
            );

            await Assert.ThrowsAsync<ArgumentNullException>(async () =>  await _securityService.SignInAsync(email, password));
        }

        [Theory]
        [InlineData("")]
        [InlineData(null)]
        public async Task ConfirmRegistrationAsync_UseIncorrectParameters_ShouldThrowsArgumentNullException(
            string token)
        {
            var passwordServiceStub = PreparePasswordServiceMock();
            var hashBuilderStub = PrepareHashBuilderMock();
            var emailNotifierStub = PrepareEmailNotifierMock();
            Mock<ILogger<SecurityService>> loggerStub = new Mock<ILogger<SecurityService>>();
            
            var dbContext = PrepareInMemoryDataContext();
            var tokenOptions = PrepareTokenOptions();
            var commonOptions = PrepareCommonOptions();
            
            _securityService = new SecurityService(
                passwordServiceStub.Object,
                hashBuilderStub.Object,
                emailNotifierStub.Object,
                loggerStub.Object,
                tokenOptions,
                commonOptions,
                dbContext
            );
            
            await Assert.ThrowsAsync<ArgumentNullException>(async () =>  await _securityService.ConfirmRegistrationAsync(token));
        }
        
        #endregion
    }
}